### STAGE 1 - BUILD ###
FROM golang:1.13 as build

WORKDIR /opt/performance-sensor

# Copy project sources
ADD . .

# build project from vendor sources
RUN go build -o performance-mqtt-sensor main.go

### STAGE 2 - FINAL ###
FROM alpine:latest

WORKDIR /opt/performance-sensor

COPY --from=build /opt/performance-sensor/performance-mqtt-sensor .

ENTRYPOINT ["/opt/performance-sensor/performance-mqtt-sensor"]

CMD []
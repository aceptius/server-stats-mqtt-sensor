package main

import (
	"context"
	"errors"
	"fmt"
	"io"
	"michalk/performance-monitor-sensor/broker"
	"michalk/performance-monitor-sensor/statistics"
	"os"
	"strconv"
	"time"
)

const (
	envClientId     = "PM_MQTT_CLIENT_ID"
	envBrokerUrl    = "PM_MQTT_BROKER_URL"
	envPublishTopic = "PM_MQTT_TOPIC"
	envHostName     = "PM_HOST_NAME"
	envInterval     = "PM_MEASUREMENT_INTERVAL"
)

type Publisher interface {
	Publish(ctx context.Context, topic string, value io.Reader) error
	PublishJson(ctx context.Context, topic string, structure interface{}) error
	Disconnect() error
}

func createPublisher() Publisher {
	option, err := broker.NewDefaultOptions(os.Getenv(envClientId), os.Getenv(envBrokerUrl))
	if err != nil {
		panic(err)
	}
	return broker.NewMqttPublisher(option)
}

func main() {
	ctx, cFunc := context.WithCancel(context.Background())
	publisher := createPublisher()
	defer func() {
		cFunc()
		_ = publisher.Disconnect()
	}()

	// load and check topic
	topic := os.Getenv(envPublishTopic)
	if topic == "" {
		panic(errors.New("publish topic can not be empty"))
	}

	// load end check interval
	interval, err := strconv.Atoi(os.Getenv(envInterval))
	if err != nil {
		panic(errors.New("interval is not valid value"))
	}

	hostName := os.Getenv(envHostName)

	ticker := time.NewTicker(time.Duration(int64(interval)) * time.Second)
	for {
		select {
		case <-ctx.Done():
			return
		case <-ticker.C:
			func() {
				stats, err := statistics.DoMeasurement(ctx, hostName)
				if err != nil {
					fmt.Println(err)
				}
				if err := publisher.PublishJson(ctx, topic, stats); err != nil {
					fmt.Println(err)
				}
			}()
		}
	}
}

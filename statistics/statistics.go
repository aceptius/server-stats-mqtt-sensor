package statistics

import (
	"context"
	"fmt"
	"github.com/shirou/gopsutil/cpu"
	"github.com/shirou/gopsutil/load"
	"github.com/shirou/gopsutil/mem"
	"time"
)

type CpuStat struct {
	Overall float64   `json:"overall"`
	PerCore []float64 `json:"per_core"`
}

type Statistics struct {
	HostName        string                 `json:"host_name"`
	MeasurementTime time.Time              `json:"measurement_time"`
	Cpu             *CpuStat               `json:"cpu"`
	Load            *load.AvgStat          `json:"load"`
	Memory          *mem.VirtualMemoryStat `json:"memory"`
	Misc            *load.MiscStat         `json:"misc"`
}

func DoMeasurement(ctx context.Context, hostName string) (*Statistics, error) {
	stats := Statistics{HostName: hostName, MeasurementTime: time.Now()}

	// cpu
	percentagePerCore, err := cpu.PercentWithContext(ctx, time.Second, true)
	if err != nil {
		return nil, fmt.Errorf("cpu measurement error : %w", err)
	}

	percentageOverall, err := cpu.PercentWithContext(ctx, time.Second, false)
	if err != nil {
		return nil, fmt.Errorf("cpu measurement error : %w", err)
	}

	stats.Cpu = &CpuStat{
		Overall: percentageOverall[0],
		PerCore: percentagePerCore,
	}

	// memory
	m, err := mem.VirtualMemoryWithContext(ctx)
	if err != nil {
		return nil, fmt.Errorf("mem reading error : %w", err)
	}
	stats.Memory = m

	// load
	l, err := load.AvgWithContext(context.Background())
	if err != nil {
		return nil, fmt.Errorf("load reading error : %w", err)
	}
	stats.Load = l

	//misc
	misc, err := load.MiscWithContext(context.Background())
	if err != nil {
		return nil, fmt.Errorf("misc reading error : %w", err)
	}
	stats.Misc = misc

	return &stats, nil
}

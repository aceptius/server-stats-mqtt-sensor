package broker

import (
	"context"
	"crypto/tls"
	json2 "encoding/json"
	"errors"
	"fmt"
	mqtt "github.com/eclipse/paho.mqtt.golang"
	"io"
	"io/ioutil"
	"net/url"
	"time"
)

const publishingErrMsg = "error publishing message: %w"

// url string example `mqtt://<user>:<pass>@<server>.cloudmqtt.com:<port>/<topic>`
func NewDefaultOptions(clientId string, brokerUrl string) (*mqtt.ClientOptions, error) {
	uri, err := url.Parse(brokerUrl)
	if err != nil {
		return nil, fmt.Errorf("wrong broker url: %w", err)
	}

	option := &mqtt.ClientOptions{ClientID: clientId, Username: uri.User.Username()}

	// add broker url
	option.AddBroker(fmt.Sprintf("tcp://%s", uri.Host))

	// set password if is set
	if pwd, ok := uri.User.Password(); ok {
		option.SetPassword(pwd)
	}

	tlsConfig := &tls.Config{InsecureSkipVerify: true, ClientAuth: tls.NoClientCert}
	option.SetTLSConfig(tlsConfig)

	return option, nil
}

func NewMqttPublisher(options *mqtt.ClientOptions) *MqttPublisher {
	return &MqttPublisher{client: mqtt.NewClient(options)}
}

type MqttPublisher struct {
	client mqtt.Client
}

func (m MqttPublisher) checkConnection(ctx context.Context) error {
	if m.client == nil {
		return errors.New("client is not defined")
	}

	if m.client.IsConnected() {
		return nil
	}

	token := m.client.Connect()
	//wait 3 second to token flow to complete
	for !token.WaitTimeout(3 * time.Second) {
		select {
		case <-ctx.Done():
			return fmt.Errorf("connection context error: %w", ctx.Err())
		}
	}

	// some error happen
	if err := token.Error(); err != nil {
		return err
	}
	return nil
}

func (m MqttPublisher) doPublish(ctx context.Context, topic string, message string) error {
	if err := m.checkConnection(ctx); err != nil {
		return fmt.Errorf(publishingErrMsg, err)
	}

	token := m.client.Publish(topic, 0, false, message)
	for !token.WaitTimeout(3 * time.Second) {
		select {
		case <-ctx.Done():
			return fmt.Errorf("publishing context error: %w", ctx.Err())
		}
	}

	if token.Error() != nil {
		return fmt.Errorf(publishingErrMsg, token.Error())
	}

	fmt.Println(fmt.Sprintf("publishing to `%s`: %s", topic, message))
	return nil
}

func (m MqttPublisher) Publish(ctx context.Context, topic string, message io.Reader) error {
	b, err := ioutil.ReadAll(message)
	if err != nil {
		return fmt.Errorf("error reading value: %w", err)
	}
	return m.doPublish(ctx, topic, string(b))
}

func (m MqttPublisher) PublishJson(ctx context.Context, topic string, structure interface{}) error {
	b, err := json2.Marshal(structure)
	if err != nil {
		return fmt.Errorf("error marshal message: %w", err)
	}
	return m.doPublish(ctx, topic, string(b))
}

func (m MqttPublisher) Disconnect() error {
	if m.client.IsConnected() {
		m.client.Disconnect(250)
	}
	return nil
}
